package cal

type Skill struct {
	ID             int    // 技能ID： 0 不操作，1 前进，2 转向， 3 普攻， 4 瞬移， 5 切换形态， 6 投掷酒精， 7 导弹打击
	Name           string // 技能名称
	FreezeTime     int    // 冷却时间
	Range          int    // 射程
	Scale          int    // 影响范围
	WaitTime       int    // 等待时间
	Damage         int    // 基础伤害值
	DamageDuration int    // 伤害持续时间
}

var SkillList = [8]Skill{
	{0, "不操作", 0, 0, 0, 0, 0, 0},
	{1, "前进", 0, 1, 0, 0, 0, 0},
	{2, "转向", 0, 0, 0, 0, 0, 0},
	{3, "普攻", 0, 2, 0, 0, 10, 1},
	{4, "瞬移", 28, 5, 0, 0, 0, 0},
	{5, "切换形态", 0, 0, 0, 0, 0, 0},
	{6, "投掷酒精", 89, 3, 3, 0, 10, 5},
	{7, "导弹打击", 54, 3, 1, 3, 30, 1},
}
