package cal

import (
	"teggame/input"
	"testing"
)

func TestRoadCal(t *testing.T) {
	inputInstance := new(input.Input)

	t.Log([]byte("2"))

	inputInstance.Robots[0] = input.Robot{1, 0, 0, 8, 0, 1, 0, 0, 0, 0, 0, 0, 0}

	var mapCols = [17]string{
		"..#.v......#........#..",
		"..#..v..#######...v.#.3",
		"..#.....#.........v.#..",
		"..#.....#...........#..",
		"..#...###########...#..",
		"..#####......v..##..#..",
		"..#..#...........#..#..",
		"..#..#v..........#..#..",
		"1....v..v..v..v..v.v...",
		"..#..#.......v...#..#..",
		"..#v.#......v....#..#..",
		"..#.v##.........#####..",
		"..#...#####v#####..v#..",
		"..#...........#.....#..",
		"..#...v.......#.....#..",
		"..#.....#######.....#.0",
		"..#........#........#..",
	}

	for i, mapCol := range mapCols {
		mapColByte := []byte(mapCol)
		for j, char := range mapColByte {
			inputInstance.Map[i][j] = char
		}
	}

	step, hasVir := RoadCal(inputInstance, 0, 1)

	t.Log(step, hasVir)

}
