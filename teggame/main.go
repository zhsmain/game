package main

import (
	"fmt"
	"log"
	"os"
	"teggame/cal"
	"teggame/input"
	"teggame/output"
)

func main() {
	file := "./app.log"
	logFile, _ := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0766)
	log.SetOutput(logFile) // 将文件设置为log输出的文件
	log.SetPrefix("[qSkipTool]")
	log.SetFlags(log.LstdFlags | log.Lshortfile | log.LUTC)
	fmt.Println("OK")
	frameMap := make(map[int]*input.Input)
	for {
		//接收输入
		inputInstance := new(input.Input)
		fmt.Scanln(&inputInstance.Id, &inputInstance.OurScore, &inputInstance.TheirScore)
		for i, _ := range inputInstance.Robots {
			fmt.Scanln(&inputInstance.Robots[i].Id, &inputInstance.Robots[i].Side, &inputInstance.Robots[i].X, &inputInstance.Robots[i].Y, &inputInstance.Robots[i].Dir, &inputInstance.Robots[i].FootStatus, &inputInstance.Robots[i].MoveFreezeTime, &inputInstance.Robots[i].TurnFreezeTime, &inputInstance.Robots[i].AttackFreezeTime, &inputInstance.Robots[i].FlashFreezeTime, &inputInstance.Robots[i].SwitchFreezeTime, &inputInstance.Robots[i].ThrowFreezeTime, &inputInstance.Robots[i].BombFreezeTime)
		}
		var mapWidth string
		var mapHeight string
		fmt.Scanln(&mapWidth, &mapHeight)
		var mapCol string
		var mapColByte []byte
		for i, _ := range inputInstance.Map {
			fmt.Scanln(&mapCol)
			mapColByte = []byte(mapCol)
			for j, char := range mapColByte {
				inputInstance.Map[i][j] = char
			}
		}
		fmt.Scanln(&inputInstance.VirusNum)
		inputInstance.VirusList = make([]input.Virus, inputInstance.VirusNum)
		for i := 0; i < inputInstance.VirusNum; i++ {
			fmt.Scanln(&inputInstance.VirusList[i].Id, &inputInstance.VirusList[i].RewardScore, &inputInstance.VirusList[i].X, &inputInstance.VirusList[i].Y, &inputInstance.VirusList[i].Hp)
		}

		fmt.Scanln(&inputInstance.SkillNum)
		inputInstance.Skills = make([]input.Skill, inputInstance.SkillNum)
		for i := 0; i < inputInstance.SkillNum; i++ {
			fmt.Scanln(&inputInstance.Skills[i].Id, &inputInstance.Skills[i].RobotId, &inputInstance.Skills[i].X, &inputInstance.Skills[i].Y, &inputInstance.Skills[i].StartId)
		}

		fmt.Scanln(&inputInstance.MsgType, &inputInstance.MsgContent)

		var finishTag string
		fmt.Scanln(&finishTag)

		frameMap[inputInstance.Id] = inputInstance

		log.Println(inputInstance)

		//输出应对动作
		process(inputInstance, frameMap)
	}
}

//inputInstance 输入参数对象
//frameMap 帧id和输入参数对象的map，可以用来查询历史帧的数据
func process(inputInstance *input.Input, frameMap map[int]*input.Input) {
	//输出这一帧输入对应的应对输出
	outputInstance := new(output.Output)
	// 设置帧ID
	outputInstance.SetId(inputInstance.Id)
	// 开始执行计算
	cal.Start(inputInstance, outputInstance)
	// 输出
	outputInstance.Exec()

	log.Println(outputInstance)
}
