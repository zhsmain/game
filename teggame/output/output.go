package output

import (
	"fmt"
	"strconv"
)

type Output struct {
	Id         int
	Action     string
	MsgType    int
	MsgContent string
}

//设置帧id
func (o *Output) SetId(id int) *Output {
	o.Id = id
	return o
}

//设置消息
//msgType 0为无附带消息，1为前往位置，2为攻击目标
//msgContent msgType=1时为坐标x,y msgType=2时为病毒id
func (o *Output) WithMsg(msgType int, msgContent string) *Output {
	o.MsgType = msgType
	o.MsgContent = msgContent
	return o
}

//执行操作
func (o *Output) Exec() {
	fmt.Println(o.Id)
	fmt.Println(o.Action)
	fmt.Println(strconv.Itoa(o.MsgType) + " " + o.MsgContent)
	fmt.Println("OK")
}

//停止
func (o *Output) Stop() *Output {
	o.Action = "0"
	return o
}

//前进
func (o *Output) Go() *Output {
	o.Action = "1"
	return o
}

// 向后转
func (o *Output) TurnBack() *Output {
	o.Action = "2 1 1"
	return o
}

//右转
func (o *Output) TurnRight() *Output {
	o.Action = "2 1"
	return o
}

//左转
func (o *Output) TurnLeft() *Output {
	o.Action = "2 -1"
	return o
}

//普攻
func (o *Output) Attack(virusId int) *Output {
	o.Action = "3 " + strconv.Itoa(virusId)
	return o
}

//瞬移
func (o *Output) Flash(x int, y int) *Output {
	o.Action = "4 " + strconv.Itoa(x) + " " + strconv.Itoa(y)
	return o
}

//切换形态，切换成轮台或战斗模式
func (o *Output) Switch() *Output {
	o.Action = "5"
	return o
}

//投掷酒精
func (o *Output) Throw(x int, y int) *Output {
	o.Action = "6 " + strconv.Itoa(x) + " " + strconv.Itoa(y)
	return o
}

//扔导弹
func (o *Output) Bomb(x int, y int) *Output {
	o.Action = "7 " + strconv.Itoa(x) + " " + strconv.Itoa(y)
	return o
}
