package input

type Robot struct {
	Id               int //机器人id
	Side             int //阵营id，0或1
	X                int //x坐标
	Y                int //y坐标
	Dir              int //朝向，0为X正⽅向，1为Y正⽅向， 2为X负⽅向，3为Y负⽅向
	FootStatus       int //当前轮⾜状态（0：⾜式，1： 轮式）
	MoveFreezeTime   int //移动冷却时间
	TurnFreezeTime   int //转向冷冻时间
	AttackFreezeTime int //普攻冷却时间
	FlashFreezeTime  int //瞬移冷冻时间
	SwitchFreezeTime int //轮式切换冷却时间
	ThrowFreezeTime  int //投掷酒精冷却时间
	BombFreezeTime   int //投掷导弹冷却时间
}

type Virus struct {
	Id          int //病毒id
	RewardScore int //击杀奖励分数
	X           int //x坐标
	Y           int //y坐标
	Hp          int //剩余血量
}

type Skill struct {
	Id      int //技能id，6为投掷酒精，7为导弹
	RobotId int //所属机器人id
	X       int //x坐标
	Y       int //y坐标
	StartId int //技能施放的起始帧id
}

type Input struct {
	Id         int          //第几帧
	OurScore   float32      //己方分数
	TheirScore float32      //敌方分数
	Robots     [4]Robot     //机器人列表
	Map        [17][23]byte //i为y坐标，j为x坐标
	VirusNum   int          //病毒数量
	VirusList  []Virus      //病毒列表
	SkillNum   int          //正在施放的技能数量
	Skills     []Skill      //正在施放的技能列表
	MsgType    int          //队友消息类型，0无消息，1前往位置x,y，2.攻击id为x的病毒
	MsgContent string       //如果MsgType=1,这个为x,y形式的坐标，如果MsgType=2,这个为要攻击的病毒id
}

//获取坐标x，y的类型
//‘.’表示地图该坐标处为空⽩，
//‘#’表示障碍物，
//‘v’表示地图该坐标处为病毒占据，
//数字0-3表示四个机器狗的位置，
func (i *Input) GetBlockType(x int, y int) byte {
	return i.Map[y][x]
}

func (ipt *Input) GetVirusInfo(x, y int) Virus {
	for _, virus := range ipt.VirusList {
		if virus.X == x && virus.Y == y {
			return virus
		}
	}
	return Virus{}
}

//最近的病毒
func (i *Input) GetClosedGateVirus() Virus {
	min := 9999
	minIndex := 0
	var tmp int
	me := i.Robots[0]
	for key, virus := range i.VirusList {
		tmp = (me.X-virus.X)*(me.X-virus.X) + (me.Y-virus.Y)*(me.Y-virus.Y)
		if tmp < min {
			min = tmp
			minIndex = key
		}
	}
	return i.VirusList[minIndex]
}
